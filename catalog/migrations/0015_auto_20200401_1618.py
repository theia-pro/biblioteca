# Generated by Django 3.0.4 on 2020-04-01 14:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0014_remove_publication_check_out_email'),
    ]

    operations = [
        migrations.AlterField(
            model_name='publication',
            name='check_out_by',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
