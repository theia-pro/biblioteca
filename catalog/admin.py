from django.contrib import admin

from .models import Publication, Profile

admin.site.site_header = 'biblioteca admin'
# admin.site.register(modelName) imports from models.py within the app

class PublicationAdmin(admin.ModelAdmin):
    list_display = ('title', 'publication_type', 'status',  'due', 'check_out_by', 'check_in_due_date',)
    list_filter = ('due', 'status', 'publication_type',)


class TransactionsAvailability(admin.ModelAdmin):
    list_display = ('user', 'books_available', 'magazines_available', 'user_type')
    list_filter = ('user_type', )


admin.site.register(Publication, PublicationAdmin)
admin.site.register(Profile, TransactionsAvailability)
