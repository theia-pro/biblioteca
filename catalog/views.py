from django.http import HttpResponse, HttpRequest
from django.urls import reverse
from django.shortcuts import render, redirect
from catalog.models import Publication, Profile
from django.urls import reverse
from django.views import generic
from urllib.parse import urlparse
from django.utils import timezone
from datetime import timedelta
from django.contrib.auth.decorators import login_required


# Create your views here.
# The @login_required decorator prevents execution if !user
@login_required
def index(request):
    if request.user.id:
        book_list = Publication.objects.all().filter(status="AV")
        context = {
            'book_list': book_list,
            'current_user': request.user,
            'profile': Profile.objects.get(user=str(request.user.id))
        }
        return render(request, 'index.html', context=context)
    else:
        return redirect('../accounts/login/')

@login_required
def loaned(HttpRequest):
    publications = Publication.objects.all().filter(status="LO", check_out_by=str(HttpRequest.user))
    for publication in publications:
        if publication.check_in_due_date <= timezone.now():
            publication.due = True
            publication.save()
        else:
            publication.due = False
            publication.save()
    context = {
        'publications': publications,
        'current_user': HttpRequest.user,
        'profile': Profile.objects.get(user=str(HttpRequest.user.id))
    }
    return render(HttpRequest, 'loaned.html', context=context)


def loan(HttpRequest, bookId):
    from datetime import timedelta
    full_path = HttpRequest.path
    separated_path = full_path.split('/')[3]
    publication_object = Publication.objects.get(id=separated_path)
    profile = Profile.objects.get(user=str(HttpRequest.user.id))
    if publication_object.publication_type == 'BO':
        if profile.books_available > 0:
            profile.books_available = profile.books_available - 1
            profile.save()
            publication_object.status = 'LO'
            publication_object.check_out_by = str(HttpRequest.user)
            publication_object.check_out_date = timezone.now()
            publication_object.check_in_due_date = timezone.now() + timedelta(days=30)
            publication_object.save()
            publications = Publication.objects.all().filter(status="AV")
            context = {
                'book_list': publications,
                'current_user': HttpRequest.user,
                'profile': Profile.objects.get(user=str(HttpRequest.user.id))
            }
            return render(HttpRequest, 'index.html', context=context)
        else:
            profile.books_available = 0
            publications = Publication.objects.all().filter(status="AV")
            context = {
                'book_list': publications,
                'current_user': HttpRequest.user,
                'profile': Profile.objects.get(user=str(HttpRequest.user.id))
            }
            return render(HttpRequest, 'index.html', context=context)
    elif publication_object.publication_type == 'MA':
        if profile.magazines_available > 0:
            profile.magazines_available = profile.magazines_available - 1
            profile.save()
            publication_object.status = 'LO'
            publication_object.check_out_by = str(HttpRequest.user)
            publication_object.check_out_date = timezone.now()
            publication_object.check_in_due_date = timezone.now() + timedelta(days=7)
            publication_object.save()
            publications = Publication.objects.all().filter(status="AV")
            context = {
                'book_list': publications,
                'current_user': HttpRequest.user,
                'profile': Profile.objects.get(user=str(HttpRequest.user.id))
            }
            return render(HttpRequest, 'index.html', context=context)

        else:
            profile.magazines_available = 0
            publications = Publication.objects.all().filter(status="AV")
            context = {
                'book_list': publications,
                'current_user': HttpRequest.user,
                'profile': Profile.objects.get(user=str(HttpRequest.user.id))
            }
            return render(HttpRequest, 'index.html', context=context)
    loaned_books = Publication.objects.all().filter(status="LO", check_out_by=str(HttpRequest.user))
    context = {
        'loaned_books': loaned_books,
        'profile': Profile.objects.get(user=str(HttpRequest.user.id))
    }
    return render(HttpRequest, 'loaned.html', context=context)

def returned(HttpRequest, bookId):
    full_path = HttpRequest.path
    separated_path = full_path.split('/')[3]
    update_status = Publication.objects.get(id=separated_path)
    update_status.status = 'AV'
    update_status.check_out_by = None
    update_status.check_out_date = None
    update_status.due = False
    update_status.save()
    profile = Profile.objects.get(user=str(HttpRequest.user.id))
    if update_status.publication_type == 'BO':
        profile.books_available = profile.books_available + 1
        profile.save()
    elif update_status.publication_type == 'MA':
        profile.magazines_available = profile.magazines_available + 1
        profile.save()
    publications = Publication.objects.all().filter(status="LO", check_out_by=str(HttpRequest.user))
    context = {
        'publications': publications,
        'current_user': HttpRequest.user,
        'profile': Profile.objects.get(user=str(HttpRequest.user.id))
    }
    return render(HttpRequest, 'loaned.html', context=context)


