from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('loaned/', views.loaned, name="loaned"),
    path('loaned/<str:bookId>/', views.loan, name="loan"),
    path('loaned/<str:bookId>/returned/', views.returned, name="returned"),
]