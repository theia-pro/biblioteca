from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Publication(models.Model):
    PUBLICATION_TYPE_CHOICES = (
        ('BO', 'Book'),
        ('MA', 'Magazine'),

    )
    STATUS_CHOICES = (
        ('AV', 'Available'),
        ('LO', 'Loaned'),
    )
    # Fields
    title = models.CharField(max_length=100, help_text='Title')
    publication_type = models.CharField(
        verbose_name='Publication Type',
        max_length=2,
        choices=PUBLICATION_TYPE_CHOICES,
        default='BO',
    )
    status = models.CharField(
        verbose_name='Book Status',
        max_length=2,
        choices=STATUS_CHOICES,
        default='AV',
    )
    check_out_by = models.CharField(max_length=100, null=True, blank=True)
    check_out_date = models.DateTimeField(null=True, blank=True)
    check_in_due_date = models.DateTimeField(null=True,blank=True)
    due = models.BooleanField(null=True)
    # Metadata

    class Meta:
        ordering = ['-title']

    def __str__(self):
        return self.title

class Profile(models.Model):
    USER_TYPES_CHOICES = (
        ('User', 'User'),
        ('Staff', 'Staff'),
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    books_available = models.IntegerField(default=10)
    magazines_available = models.IntegerField(default=3)

    user_type = models.CharField(
        max_length=5,
        choices=USER_TYPES_CHOICES,
        default='User',
    )

# Create a Profile when a new user is created START
# From https://simpleisbetterthancomplex.com/tutorial/2016/07/22/how-to-extend-django-user-model.html#onetoone
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
# Create a Profile when a new user is created END

