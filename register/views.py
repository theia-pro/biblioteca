from django.shortcuts import render, redirect
from .forms import RegisterForm
from django.contrib import auth
from django.contrib.auth import authenticate, login


def register(response):
    if response.method == "POST":
        signup_form = RegisterForm(response.POST)
        if signup_form.is_valid():
            signup_form.save()
            new_user = authenticate(username=signup_form.cleaned_data['username'],
                                    password=signup_form.cleaned_data['password1'],
                                    )
            login(response, new_user)
            # redirect to home. There is a redirect to /catalog
            return redirect("/")
    else:
        signup_form = RegisterForm()

    return render(response, "register/register.html", {"form": signup_form})

def logout(request):
    auth.logout(request)
    return redirect("/accounts/login/")
