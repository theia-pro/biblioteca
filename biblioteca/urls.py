from django.contrib import admin
from django.urls import path, include
from django.views.generic import RedirectView
from register import views as v


urlpatterns = [
    path('', RedirectView.as_view(url='catalog/', permanent=True)),
    path('admin/', admin.site.urls),
    path('catalog/', include('catalog.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('register/', v.register, name="register"),
    path(r'^logout/$', v.logout, name='logout'),
]
